<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreCarDetailsToCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars',function($table){
            $table->integer('mileage');
            $table->string('fuel_type');
            $table->integer('engine');
            $table->string('transmission');
            $table->integer('stock_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars',function($table){
            $table->dropColumn('mileage');
            $table->dropColumn('fuel_type');
            $table->dropColumn('engine');
            $table->dropColumn('transmission');
            $table->dropColumn('stock_no');
        });
    }
}
