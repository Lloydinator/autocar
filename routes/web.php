<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');
Route::get('/about', 'PageController@about');
Route::get('/contact', 'PageController@contact');
Route::get('/privacy', 'PageController@privacy');
Route::get('/services', 'PageController@services');
Route::get('/services/oil-change', 'PageController@oil');
Route::get('/services/exhaust-mufflers', 'PageController@mufflers');
Route::get('/services/brakes', 'PageController@brakes');
Route::get('/services/tires-wheels', 'PageController@wheels');
Route::get('/services/ac', 'PageController@ac');
Route::get('/services/steering-suspension', 'PageController@steering');
Route::get('/services/car-battery', 'PageController@battery');
Route::get('/services/cv-joints', 'PageController@cv');
Route::get('/terms', 'PageController@terms');
Route::get('/cars', 'PageController@cars');
Route::get('/third-party', 'PageController@thirdparty');
//Route::get('/cars/{$category?}', 'PageController@cars');

Route::resource('car','PageController')->only(['show']);

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('admin');
Route::resource('admin/content', 'ContentController');
Route::resource('admin/cars', 'CarsController');
Route::get('admin/cars/gallery/{id}','CarsController@show');
Route::get('admin/cars/gallery/{id}/create','GalleryController@create');
Route::post('admin/cars/gallery/store','GalleryController@store');
Route::delete('admin/cars/gallery/{id}','GalleryController@destroy');
//Route::resource('admin/gallery','GalleryController');
/*
Route::group(['prefix' => 'admin/gallery'],function(){
    Route::get('/{id}', 'GalleryController@index');
    Route::match(['get','post'], 'create', 'GalleryController@create');
    Route::match(['get','put'], 'update/{id}', 'GalleryController@update');
    Route::delete('delete/{id}','GalleryController@delete');
});
*/

Route::get('/cars/sedans', 'PageController@sedans');
Route::get('/cars/hybrids', 'PageController@hybrids');
Route::get('/cars/compact', 'PageController@compact');
Route::get('/cars/electric', 'PageController@electric');
Route::get('/cars/convertibles', 'PageController@convertibles');
Route::get('/cars/luxury', 'PageController@luxury');
Route::get('/cars/crossovers', 'PageController@crossovers');
Route::get('/cars/suvs', 'PageController@suvs');
Route::get('/cars/vans', 'PageController@vans');
Route::get('/cars/trucks', 'PageController@trucks');
