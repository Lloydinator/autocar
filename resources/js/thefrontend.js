import {TweenMax,TimelineMax,ScrollToPlugin} from "gsap/all";
require('./plugins/OwlCarousel2-2.2.1/owl.carousel');
require('./plugins/slick-1.8.0/slick');
require('./plugins/easing/easing');
require('./plugins/Isotope/isotope.pkgd.min.js');
require('./plugins/parallax-js-master/parallax.min.js');
require('./plugins/custom');

/*
<script src="js/plugins/greensock/TweenMax.min.js"></script>
<script src="js/plugins/greensock/TimelineMax.min.js"></script>
<script src="js/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="js/plugins/greensock/animation.gsap.min.js"></script>
<script src="js/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="js/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="js/plugins/slick-1.8.0/slick.js"></script>
<script src="js/plugins/easing/easing.js"></script>
<script src="js/plugins/custom.js"></script>
<script src="js/plugins/Isotope/isotope.pkgd.min.js"></script>
<!--script src="js/plugins/jquery-ui-1.12.1.custom/jquery-ui.js"></script-->
<script src="js/plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/plugins/shop_custom.js"></script>
*/