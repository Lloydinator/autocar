@extends('layouts/admin-index')

@section('content')
    <h1>Create Page</h1>
    {!! Form::open(['action' => 'ContentController@store','method'=>'POST','enctype'=>'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('title','Title')}}
            {{Form::text('title','',['class'=>'form-control','placeholder'=>'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('body','Body')}}
            {{Form::textarea('body','',['class'=>'form-control','placeholder'=>'Type your stuff here...'])}}
        </div>
        <div class="form-group">
            {{Form::label('content-page','Page')}}
            {{Form::select('content-page',['about'=>'About',
                                            'contact'=>'Contact',
                                            'privacy'=>'Privacy',
                                            'services'=>'Services',
                                            'terms'=>'Terms',
                                            'services-mufflers'=>'Mufflers',
                                            'services-oil'=>'Oil',
                                            'services-brakes'=>'Brakes',
                                            'services-wheels'=>'Wheels',
                                            'services-ac'=>'A/C',
                                            'services-steering'=>'Steering Wheel',
                                            'services-battery'=>'Battery',
                                            'services-cv'=>'CV Joints',
                                            ],['class'=>'form-control','placeholder'=>'Page'])}}
        </div>
        <div class="form-group">
            {{Form::label('image','Main Image')}}
            {{Form::file('image')}}
        </div>
        {{Form::submit('Send',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection