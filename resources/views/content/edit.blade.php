@extends('layouts/admin-index')

@section('content')
<h1>Edit {{$content->page}}</h1>
{!! Form::open(['action' => ['ContentController@update',$content->id],'method'=>'POST','enctype'=>'multipart/form-data']) !!}
    <div class="form-group">
        {{Form::label('title','Title')}}
        {{Form::text('title',$content->title,['class'=>'form-control','placeholder'=>'Title'])}}
    </div>
    <div class="form-group">
        {{Form::label('body','Body')}}
        {{Form::textarea('body',$content->body,['id'=>'article-ckeditor','class'=>'form-control','placeholder'=>'Type your stuff here...'])}}
    </div>
    <div class="form-group">
            {{Form::label('image','Main Image')}}
            {{Form::file('image')}}
        </div>
    <p>
    {{Form::hidden('_method','PUT')}} {{-- This ensures that it updates--}}
    {{Form::submit('Send',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
    
    {!!Form::open(['action'=>['ContentController@destroy',$content->id],'method'=>'POST','class'=>'float-right'])!!}
    {{Form::hidden('_method','DELETE')}}
    {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
    {!! Form::close() !!}
    </p>
@endsection