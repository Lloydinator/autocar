@extends('layouts/admin-index')

@section('content')
    @if(!Auth::guest())
        <div class="table-responsive">
            @if (count($contents)>0)
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th></th>
                        <th><a href="/admin/content/create" class="btn btn-light">+</a></th>
                    </tr>
                </thead>
                @foreach($contents as $content)
                    <tbody>
                        <tr>
                            <td>{{$content->page}}</td>
                            <td><a href="/admin/content/{{$content->id}}/edit" class="btn btn-primary">Edit</a></td>
                        </tr>
                    </tbody>
                @endforeach
            </table>
            @else 
                <p>You have no posts.</p>
            @endif
        </div>
    @endif
@endsection

