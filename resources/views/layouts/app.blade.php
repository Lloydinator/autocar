<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<title>Auto Trend Motorcars | The one stop shop for great looking cars, collision repairs and motor repairs</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Auto Trends Motorcars is the best place in the Bronx to buy cars, get great car service and car repairs.">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Fonts -->
<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

<!-- Styles -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
<link href="{{ asset('css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{asset('css/thefrontend.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/outdated.css')}}">
<style>
.footer .logo
{
	top: auto;
	-webkit-transform: transform: none;
    -moz-transform: transform: none;
    -ms-transform: transform: none;
    -o-transform: transform: none;
    transform: none;
}
</style>
</head>

<body>
<div class="app super_container">
	@include('includes/navbar')
	<main class="py-4">
		@include('includes/messages')
		@yield('content')
	</main>
	@include('includes/footer')
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{ asset('js/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{ asset('js/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('js/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ asset('js/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
<script src="{{ asset('js/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ asset('js/plugins/slick-1.8.0/slick.js') }}"></script>
<script src="{{ asset('js/plugins/easing/easing.js') }}"></script>
<script src="{{ asset('js/plugins/Isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('js/plugins/parallax-js-master/parallax.min.js') }}"></script>
<script src="{{ asset('js/plugins/shop_custom.js') }}"></script>
<script src="{{ asset('js/plugins/custom.js') }}"></script>
<script>
$(document).ready(function(){
	//Call function
	initImage();

	//Function to switch 
	function initImage(){
		var images = $('.image_list li');
		var selected = $('.image_selected img');

		images.each(function(){
			var image = $(this);
			image.on('click', function(){
				var imagePath = new String(image.data('image'));
				selected.attr('src', imagePath);
			});
		});
	}
});

// Plain Javascript
//event listener: DOM ready
function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}
//call plugin function after DOM ready
addLoadEvent(function(){
    outdatedBrowser({
        bgColor: '#f25648',
        color: '#ffffff',
        lowerThan: 'transform',
        languagePath: '/lang/en.html'
    })
});

//Google Maps
function initMap() {
  // The location of Uluru
  var royCar = {lat: 40.901763, lng: -73.850959};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 16, center: royCar});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: royCar, map: map});
}

</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAw79kHR3uWcnQyD1Opn-sQAY-nD56xOzA&callback=initMap">
</script>
</body>

</html>
