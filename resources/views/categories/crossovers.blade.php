@extends('layouts/app')

@section('content')
	<!-- Home -->

	<div class="home">
            <div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/shop_background.jpg"></div>
            <div class="home_overlay"></div>
            <div class="home_content d-flex flex-column align-items-center justify-content-center">
                <h2 class="home_title">All Crossovers</h2>
            </div>
        </div>
    
        <!-- Shop -->
    
        <div class="shop">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
    
                        <!-- Shop Sidebar -->
                        <div class="shop_sidebar">
                            <div class="sidebar_section">
                                <div class="sidebar_title">Categories</div>
                                <ul class="sidebar_categories">
                                    <li><a href="/cars/sedans">Sedans</a></li>
                                    <li><a href="/cars/hatchbacks">Hatchbacks</a></li>
                                    <li><a href="/cars/compact">Compact</a></li>
                                    <li><a href="/cars/hatchbacks">Hatchbacks</a></li>
                                    <li><a href="/cars/compact">Compact</a></li>
                                    <li><a href="/cars/suvs">SUVs</a></li>
                                    <li><a href="/cars/vans">Vans</a></li>
                                    <li><a href="/cars/trucks">Trucks</a></li>
                                </ul>
                            </div>
                            <div class="sidebar_section">
                                <div class="sidebar_subtitle brands_subtitle">Manufacturers</div>
                                <ul class="brands_list">
                                    <li class="brand"><a href="#">Acura</a></li>
                                    <li class="brand"><a href="#">Cadillac</a></li>
                                    <li class="brand"><a href="#">Hevy</a></li>
                                    <li class="brand"><a href="#">Ford</a></li>
                                    <li class="brand"><a href="#">Honda</a></li>
                                    <li class="brand"><a href="#">Kia</a></li>
                                    <li class="brand"><a href="#">Nissan</a></li>
                                    <li class="brand"><a href="#">Toyota</a></li>
                                </ul>
                            </div>
                        </div>
    
                    </div>
    
                    <div class="col-lg-9">
                        
                        <!-- Shop Content -->
    
                        <div class="shop_content">
                            <div class="shop_bar clearfix">
                                <div class="product_count"><span>{{count($crossovers)}}</span> vehicles found</div>
                                <div class="shop_sorting">
                                    <span>Sort by:</span>
                                    <ul>
                                        <li>
                                            <span class="sorting_text">highest rated<i class="fas fa-chevron-down"></span></i>
                                            <ul>
                                                <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>highest rated</li>
                                                <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>name</li>
                                                <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>price</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
    
                            <div class="product_grid">
                                <div class="product_grid_border"></div>
    
                                <!-- Product Item -->
                                @if (count($crossovers)>0)
                                    @foreach ($crossovers as $car)
                                        <div class="product_item">
                                            <div class="product_border"></div>
                                        <div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="/storage/car_img/{{$car->img}}" alt="{{$car->car_name}}"></div>
                                            <div class="product_content">
                                                <div class="product_price">{{$car->price}}</div>
                                            <div class="product_name"><div><a href="/car/{{$car->id}}" tabindex="0">{{$car->car_name}}</a></div></div>
                                            </div>
                                            <div class="product_fav"><i class="fas fa-heart"></i></div>
                                        </div>
                                    @endforeach
                                @else
                                    <h3>There's nothing to see here.</h3>
                                @endif
    
                            </div>
    
                        </div>
    
                    </div>
                </div>
            </div>
        </div>
    
@endsection