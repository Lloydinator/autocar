@extends('layouts/admin-index')

@section('content')
    <h1>Edit Car</h1>
    {!! Form::open(['action' => ['CarsController@update',$car->id],'method'=>'POST','enctype'=>'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('type','Type')}}
            {{Form::text('type',$car->car_type,['class'=>'form-control','placeholder'=>'Car Type'])}}
        </div>
        <div class="form-group">
            {{Form::label('name','Name')}}
            {{Form::text('name',$car->car_name,['class'=>'form-control','placeholder'=>'Car Name'])}}
        </div>
        <div class="form-group">
            {{Form::label('price','Price')}}
            {{Form::text('price',$car->price,['class'=>'form-control','placeholder'=>'Car Price'])}}
        </div>
        <div class="form-group">
            {{Form::label('year','Year')}}
            {{Form::text('year',$car->year,['class'=>'form-control','placeholder'=>'eg. 2016'])}}
        </div>
        <div class="form-group">
            {{Form::label('condition','Condition')}}
            {{Form::text('condition',$car->condition,['class'=>'form-control','placeholder'=>'eg. Excellent'])}}
        </div>
        <div class="form-group">
            {{Form::label('mileage','Mileage')}}
            {{Form::text('mileage',$car->mileage,['class'=>'form-control','placeholder'=>'Mileage'])}}
        </div>
        <div class="form-group">
            {{Form::label('fuel','Fuel Type')}}
            {{Form::text('fuel',$car->fuel_type,['class'=>'form-control','placeholder'=>'Fuel Type'])}}
        </div>
        <div class="form-group">
            {{Form::label('engine','Engine')}}
            {{Form::text('engine',$car->engine,['class'=>'form-control','placeholder'=>'Engine'])}}
        </div>
        <div class="form-group">
            {{Form::label('transmission','Transmission')}}
            {{Form::text('transmission',$car->transmission,['class'=>'form-control','placeholder'=>'Transmission'])}}
        </div>
        <div class="form-group">
            {{Form::label('stock','Stock #')}}
            {{Form::text('stock',$car->stock_no,['class'=>'form-control','placeholder'=>'Stock #'])}}
        </div>
        <div class="form-group">
            {{Form::label('details','Details')}}
            {{Form::textarea('details',$car->details,['id'=>'article-ckeditor','class'=>'form-control','placeholder'=>'Type your stuff here...'])}}
        </div>
        <div class="form-group">
            {{Form::file('car_img')}}
        </div>
        <p>
        {{Form::hidden('_method','PUT')}} {{-- This ensures that it updates--}}
        {{Form::submit('Send',['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
        
        {!!Form::open(['action'=>['CarsController@destroy',$car->id],'method'=>'POST','class'=>'float-right'])!!}
        {{Form::hidden('_method','DELETE')}}
        {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
        {!! Form::close() !!}
        </p>
@endsection