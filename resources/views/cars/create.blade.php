@extends('layouts/admin-index')

@section('content')
<h1>Create Car</h1>
{!! Form::open(['action' => 'CarsController@store','method'=>'POST','enctype'=>'multipart/form-data']) !!}
    <div class="form-group">
        {{Form::label('type','Type')}}
        {{Form::text('type','',['class'=>'form-control','placeholder'=>'eg. Sedan'])}}
    </div>
    <div class="form-group">
        {{Form::label('name','Name')}}
        {{Form::text('name','',['class'=>'form-control','placeholder'=>'eg. Nissan Versa'])}}
    </div>
    <div class="form-group">
        {{Form::label('price','Price')}}
        {{Form::text('price','',['class'=>'form-control','placeholder'=>'eg. 30000'])}}
    </div>
    <div class="form-group">
        {{Form::label('year','Year')}}
        {{Form::text('year','',['class'=>'form-control','placeholder'=>'eg. 2016'])}}
    </div>
    <div class="form-group">
        {{Form::label('condition','Condition')}}
        {{Form::text('condition','',['class'=>'form-control','placeholder'=>'eg. Excellent'])}}
    </div>
    <div class="form-group">
        {{Form::label('mileage','Mileage')}}
        {{Form::text('mileage','',['class'=>'form-control','placeholder'=>'eg. 200'])}}
    </div>
    <div class="form-group">
        {{Form::label('fuel','Fuel Type')}}
        {{Form::text('fuel','',['class'=>'form-control','placeholder'=>'Gasoline'])}}
    </div>
    <div class="form-group">
        {{Form::label('engine','Engine')}}
        {{Form::text('engine','',['class'=>'form-control','placeholder'=>'3000'])}}
    </div>
    <div class="form-group">
        {{Form::label('transmission','Transmission')}}
        {{Form::text('transmission','',['class'=>'form-control','placeholder'=>'Automatic'])}}
    </div>
    <div class="form-group">
        {{Form::label('stock','Stock #')}}
        {{Form::text('stock','',['class'=>'form-control','placeholder'=>'239188'])}}
    </div>
    <div class="form-group">
        {{Form::label('details','Details')}}
        {{Form::textarea('details','',['id'=>'article-ckeditor','class'=>'form-control','placeholder'=>'Type your stuff here...'])}}
    </div>
    <div class="form-group">
        {{Form::label('car_img','Main Image')}}
        {{Form::file('car_img')}}
    </div>
    {{Form::submit('Save',['class'=>'btn btn-primary'])}}
{!! Form::close() !!}
@endsection