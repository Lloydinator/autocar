@extends('layouts/admin-index')

@section('content')
    <!-- column -->
    
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Your Cars</h4>
                <div class="table-responsive">
                    <table class="table table-striped">
                        @if (count($cars)>0)
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th></th>
                                <th><a href="/admin/cars/create" class="btn btn-light">+</a></th>
                            </tr>
                        </thead>
                        @foreach($cars as $car)
                            <tbody>
                                <tr>
                                    <td>{{$car->car_name}}</td>
                                    <td><a href="/admin/cars/{{$car->id}}/edit" class="btn btn-primary">Edit</a></td>
                                    <td><a href="/admin/cars/gallery/{{$car->id}}" class="btn btn-primary">Add Photo</a></td>
                                </tr>
                            </tbody>
                        @endforeach
                        @else 
                            <p>You have no posts.</p>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

