@extends('layouts/admin-index')

@section('content')
<a href="/admin/cars/gallery/{{$car_id}}">Back</a><br><br>
<h1>Create More Photos</h1>
{!! Form::open(['action' => 'GalleryController@store','method'=>'POST','enctype'=>'multipart/form-data']) !!}
    <div class="form-group">
        {{Form::label('title','Title')}}
        {{Form::text('title','',['class'=>'form-control'])}}
    </div>
    <div class="form-group">
        {{Form::label('caption','Caption')}}
        {{Form::textarea('caption','',['id'=>'article-ckeditor','class'=>'form-control'])}}
    </div>
    <div class="form-group">
        {{Form::label('gallery_image','Add Photo')}}
        {{Form::file('gallery_image')}}
    </div>
    {{Form::hidden('car_id',$car_id)}}
    {{Form::submit('Save',['class'=>'btn btn-primary'])}}
{!! Form::close() !!}
@endsection