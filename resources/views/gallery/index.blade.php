@extends('layouts/admin-index')

@section('content')
    <!-- column -->
    
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">{{ $car->car_name }} Photos</h4>
                <div class="table-responsive">
                    @if (count($car->gallery)>0)
                    <table style="width: 100%" class="table-striped">
                        @foreach ($car->gallery as $photo)
                        <tr>
                            <td>
                                <img src="/storage/car_gallery/{{ $photo->gallery_photo }}" width="150px">
                            </td>
                            <td>
                                {!!Form::open(['action'=>['GalleryController@destroy',$photo->id],'method'=>'POST'])!!}
                                {{Form::hidden('_method','DELETE')}}
                                {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @else
                        <p>No photos here.</p>
                    @endif
                </div>
            </div>
            <a href="{{$car->id}}/create" class="btn btn-light">Add more photos</a>
        </div>
    </div>
</div>
@endsection

