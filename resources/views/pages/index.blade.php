@extends('layouts/app')

@section('content')
<!-- Carousel -->
	<div id="carouselExampleIndicators" class="carousel slide glandular" data-ride="carousel" data-interval="7200">
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
		</ol>
		<div class="carousel-inner glandular-pro">
			<div class="carousel-item active">
				<img class="d-block w-100" src="images/nicolai-berntsen-19116-unsplash_edit.jpg" alt="First slide">
				<div class="carousel-caption bracka">
					<div class="snapcrap">
						<h1 class="text-uppercase">Ride in fine style</h1>
						<p>...</p>
					</div>
				</div>
					
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="images/ant-miner-722665-unsplash_edit.jpg" alt="Second slide">
				<div class="carousel-caption bracka">
					<div class="snapcrap">
						<h1 class="text-uppercase">We've got great cars</h1>
						<p>...</p>
					</div>
				</div>
					
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="images/jack-ward-581344-unsplash_edit.jpg" alt="Third slide">
				<div class="carousel-caption bracka">
					<div class="snapcrap">
						<h1 class="text-uppercase">Big...</h1>
						<p>...</p>
					</div>
				</div>
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="images/cam-bowers-124438-unsplash_edit.jpg" alt="Fourth slide">
				<div class="carousel-caption bracka">
					<div class="snapcrap">
						<h1 class="text-uppercase">... and small</h1>
						<p>...</p>
					</div>
				</div>	
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="images/autotrend_collisionrepair_edit.jpg" alt="Fifth slide">
				<div class="carousel-caption bracka">
					<div class="snapcrap">
						<h1 class="text-uppercase">We do Collision Repairs!</h1>
						<p>...</p>
					</div>
				</div>	
			</div>
		</div>
	</div>
  



	<!-- Popular Categories -->

	<div class="popular_categories primary-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="popular_categories_content">
						<h2 class="popular_categories_title">Categories</h2>
						<div class="popular_categories_slider_nav">
							<div class="popular_categories_prev popular_categories_nav"><i class="fas fa-angle-left ml-auto"></i></div>
							<div class="popular_categories_next popular_categories_nav"><i class="fas fa-angle-right ml-auto"></i></div>
						</div>
						<div class="popular_categories_link"><a href="/cars">All Categories</a></div>
					</div>
				</div>
				
				<!-- Popular Categories Slider -->

				<div class="col-lg-9">
					<div class="popular_categories_slider_container">
						<div class="owl-carousel owl-theme popular_categories_slider owl-drag">

							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_sedan.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/sedans">Sedans</a></div>
								</div>
							</div>

							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_hybrid.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/hybrids">Hybrids</a></div>
								</div>
							</div>

							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_compact.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/compact">Compact</a></div>
								</div>
							</div>

							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_crossover.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/crossovers">Crossovers</a></div>
								</div>
							</div>
                                              
							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_electric.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/electric">Electric</a></div>
								</div>
							</div>

							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_convertible.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/convertibles">Convertibles</a></div>
								</div>
							</div>

							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_luxury.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/luxury">Luxury</a></div>
								</div>
							</div>

							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_suv.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/suvs">SUVs</a></div>
								</div>
							</div>

							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_van.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/vans">Vans</a></div>
								</div>
							</div>

							<!-- Popular Categories Item -->
							<div class="owl-item">
								<div class="popular_category d-flex flex-column align-items-center justify-content-center">
									<div class="popular_category_image"><img src="images/popular_truck.png" alt=""></div>
									<div class="popular_category_text"><a href="/cars/trucks">Trucks</a></div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Characteristics -->

	<div class="characteristics tertiary-bg">
		<div class="container">
			<div class="row">

				<!-- Char. Item -->
				<div class="col-lg-3 col-md-6 char_col">
					
					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_1.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Like Brand New</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-3 col-md-6 char_col">
					
					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_2.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Fast Turnaround</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-3 col-md-6 char_col">
					
					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_3.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Affordable Rates</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-3 col-md-6 char_col">
					
					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_4.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Cars For Sale</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="remember-me"><h2 class="before-after">Here's a before & after...</h2></div>				
					<div class="repair-columns d-flex flex-row align-items-start justify-content-between">
						<div class="repair-col">
							<div class="blog_image" style="background-image:url(images/before.png)"></div>
							<p>Our experienced staff is here to make sure your car is back to pre-accident condition, in the shortest possible time. We utilize state-of-the-art equipment, including high-tech diagnostic tools, frame straightening equipment, computerized color-matching systems, heated paint booths and more. We guarantee all of our products and services. So trust that your car is in good hands at Autotrend.</p>
						</div>
						<div class="repair-col">
							<div class="blog_image" style="background-image:url(images/after.png)"></div>
							<p>When it comes to superior collision repair and exemplary automotive refinishing, you can count on us. At AutoTrend Collision Center, we strive hard to ensure customer satisfaction through process improvement, performance consistency and our dedication to quality and safety. </p>
							<!--div class="blog_button"><a href="blog_single.html">Continue Reading</a></div-->
						</div>
					</div> 
				</div>
			</div>
		</div>


	<!-- Trends -->

	<div class="trends">
		<div class="trends_background" style="background-image:url(images/trends_background.jpg)"></div>
		<div class="trends_overlay primary-bg"></div>
		<div class="container">
			<div class="row">

				<!-- Trends Content -->
				<div class="col-lg-3">
					<div class="trends_container">
						<h2 class="trends_title">AUTOTREND REPAIR SERVICES</h2>
						<div class="trends_text"><p>We do a variety of things to keep your car in top shape.</p></div>
						<div class="trends_slider_nav">
							<div class="trends_prev trends_nav"><i class="fas fa-angle-left ml-auto"></i></div>
							<div class="trends_next trends_nav"><i class="fas fa-angle-right ml-auto"></i></div>
						</div>
					</div>
				</div>

				<!-- Trends Slider -->
				<div class="col-lg-9">
					<div class="trends_slider_container">

						<!-- Trends Slider -->

						<div class="owl-carousel owl-theme trends_slider">

							<!-- Trends Slider Item -->
							<div class="owl-item">
								<div class="trends_item">
									<div class="trends_image d-flex flex-column align-items-center justify-content-center"><img src="images/trend_oil1.jpg" alt=""></div>
									<div class="trends_content">
										<div class="trends_info clearfix">
											<h4 class="text-center"><a href="/services/oil-change">OIL CHANGE</a></h4>
										</div>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="trends_item">
									<div class="trends_image d-flex flex-column align-items-center justify-content-center"><img src="images/trend_muffler.jpg" alt=""></div>
									<div class="trends_content">
										<div class="trends_info clearfix">
											<h4 class="text-center"><a href="/services/exhaust-mufflers">EXHAUST & MUFFLERS</a></h4>
										</div>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="trends_item">
									<div class="trends_image d-flex flex-column align-items-center justify-content-center"><img src="images/trend_brake.jpg" alt=""></div>
									<div class="trends_content">
										<div class="trends_info clearfix">
											<h4 class="text-center"><a href="/services/brakes">BRAKES</a></h4>
										</div>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="trends_item">
									<div class="trends_image d-flex flex-column align-items-center justify-content-center"><img src="images/trend_wheel1.jpg" alt=""></div>
									<div class="trends_content">
										<div class="trends_info clearfix">
											<h4 class="text-center"><a href="/services/tires-wheels">TIRES & WHEELS</a></h4>
										</div>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="trends_item">
									<div class="trends_image d-flex flex-column align-items-center justify-content-center"><img src="images/trend_ac.jpg" alt=""></div>
									<div class="trends_content">
										<div class="trends_info clearfix">
											<h4 class="text-center"><a href="/services/ac">A/C</a></h4>
										</div>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="trends_item">
									<div class="trends_image d-flex flex-column align-items-center justify-content-center"><img src="images/trend_steeringwheel.jpg" alt=""></div>
									<div class="trends_content">
										<div class="trends_info clearfix">
											<h4 class="text-center"><a href="/services/steering-suspension">STEERING & SUSPENSION</a></h4>
										</div>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="trends_item">
									<div class="trends_image d-flex flex-column align-items-center justify-content-center"><img src="images/trend_battery.jpg" alt=""></div>
									<div class="trends_content">
										<div class="trends_info clearfix">
											<h4 class="text-center"><a href="/services/car-battery">BATTERIES</a></h4>
										</div>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="trends_item">
									<div class="trends_image d-flex flex-column align-items-center justify-content-center"><img src="images/trend_cv.jpg" alt=""></div>
									<div class="trends_content">
										<div class="trends_info clearfix">
											<h4 class="text-center"><a href="/services/cv-joints">CV JOINTS</a></h4>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Banner -->
	<div class="banner">
		<div class="banner_background" style="background-image:url(images/banner_background.jpg)"></div>
		<div class="container-fluid fill_height">
			<div class="row fill_height">
				<div class="banner_product_image"><img src="images/benz-c-class_edit.png" alt=""></div>
				<div class="col-lg-5 offset-lg-4 fill_height">
					<div class="banner_content">
						<h1 class="animated bounceInLeft delay-5s slower banner_text">new era of vehicles</h1>
						<div class="animated bounceInLeft delay-5s slower banner_product_name">2017 Mercedes Benz C-Class</div>
						<div class="button banner_button"><a href="/cars">Details</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Deals of the week -->

	<div class="deals_featured primary-bg">
		<div class="container">
			<div class="row">
				<div class="col d-flex flex-lg-row flex-column align-items-center justify-content-start">
					
					<!-- Deals -->

					<div class="deals">
						<div class="deals_title">Deals of the Week</div>
						<div class="deals_slider_container">
							
							<!-- Deals Slider -->
							<div class="owl-carousel owl-theme deals_slider">
								
								<!-- Deals Item -->
								<div class="owl-item deals_item">
									<div class="deals_image"><img src="images/rangeroversport2017.png" alt=""></div>
									<div class="deals_content">
										<div class="deals_info_line d-flex flex-row justify-content-start">
											<div class="deals_item_category"><a href="/cars/suvs">SUVs</a></div>
										</div>
										<div class="deals_info_line d-flex flex-row justify-content-start">
											<div class="deals_item_name">2017 Range Rover Sport</div>
										</div>
										<div class="deals_timer d-flex flex-row align-items-center justify-content-start">
											<div class="deals_timer_title_container">
												<div class="deals_timer_title">Hurry Up</div>
												<div class="deals_timer_subtitle">Offer ends in:</div>
											</div>
											<div class="deals_timer_content ml-auto">
												<div class="deals_timer_box clearfix" data-target-time="October 12, 2018">
													<div class="deals_timer_unit">
														<div id="deals_timer1_hr" class="deals_timer_hr"></div>
														<span>hours</span>
													</div>
													<div class="deals_timer_unit">
														<div id="deals_timer1_min" class="deals_timer_min"></div>
														<span>mins</span>
													</div>
													<div class="deals_timer_unit">
														<div id="deals_timer1_sec" class="deals_timer_sec"></div>
														<span>secs</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>

						<!--div class="deals_slider_nav_container">
							<div class="deals_slider_prev deals_slider_nav"><i class="fas fa-chevron-left ml-auto"></i></div>
							<div class="deals_slider_next deals_slider_nav"><i class="fas fa-chevron-right ml-auto"></i></div>
						</div-->
					</div>
					
					<!-- Featured -->
					<div class="featured">
						<div class="tabbed_container">
							<div class="tabs">
								<ul class="clearfix">
									<li class="active">Featured Cars</li>
								</ul>
								<div class="tabs_line"><span></span></div>
							</div>

							<!-- Product Panel -->
							<div class="product_panel active">
								<div class="featured_slider slider">

									<!-- Slider Item -->
									@if (count($cars)>0)
									@foreach ($cars as $car)
									<div class="featured_slider_item">
										<div class="product_item primary-bg is_new d-flex flex-column align-items-center justify-content-center text-center">
											<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="/storage/car_img/{{$car->img}}" alt=""></div>
											<div class="product_content">
												<div class="product_name"><div><a href="/car/{{$car->id}}">{{$car->car_name}}</a></div></div>
											</div>
										</div>
									</div>
									@endforeach
									@endif

								</div>
								<div class="featured_slider_dots_cover"></div>
							</div>

							<!-- Product Panel -->

							<div class="product_panel">
								<div class="featured_slider slider">

									<!-- Slider Item -->
									@if (count($cars)>0)
									@foreach ($cars as $car)
									<div class="featured_slider_item">
										<div class="border_active"></div>
										<div class="product_item tertiary-bg is_new d-flex flex-column align-items-center justify-content-center text-center">
											<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="/storage/car_img/{{$car->img}}" alt=""></div>
											<div class="product_content">
												<div class="product_price">${{$car->price}}</div>
												<div class="product_name"><div><a href="/car/{{$car->id}}">{{$car->car_name}}</a></div></div>
											</div>
										</div>
									</div>
									@endforeach
									@endif
								</div>
								<div class="featured_slider_dots_cover"></div>
							</div>
							<!-- Product Panel -->

							<div class="product_panel">
								<div class="featured_slider slider">

									<!-- Slider Item -->
									@if (count($cars)>0)
									@foreach ($cars as $car)
									<div class="featured_slider_item">
										<div class="border_active"></div>
										<div class="product_item tertiary-bg is_new d-flex flex-column align-items-center justify-content-center text-center">
											<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="/storage/car_img/{{$car->img}}" alt=""></div>
											<div class="product_content">
												<div class="product_price">${{$car->price}}</div>
											<div class="product_name"><div><a href="/car/{{$car->id}}">{{$car->car_name}}</a></div></div>
											</div>
										</div>
									</div>
									@endforeach
									@endif

								</div>
								<div class="featured_slider_dots_cover"></div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- Reviews -->

	<div class="reviews tertiary-bg">
		<div class="container">
			<div class="row">
				<div class="col">
					
					<div class="reviews_title_container">
						<h3 class="reviews_title">SATISFIED CUSTOMERS</h3>
					</div>

					<div class="reviews_slider_container">
						
						<!-- Reviews Slider -->
						<div class="owl-carousel owl-theme reviews_slider">
							
							<!-- Reviews Slider Item -->
							<div class="owl-item">
								<div class="review d-flex flex-row align-items-start justify-content-start">
									<div><div class="review_image"><img src="images/placeholder_man.png" alt=""></div></div>
									<div class="review_content">
										<div class="review_name">Adam C.</div>
										<div class="review_rating_container">
											<div class="review_rating">@for ($i=1; $i<=5; $i++) <i class="fas fa-star"></i>@endfor</div>
										</div>
										<div class="review_text"><p>I can't sing the praises of this place enough. The owner is very friendly and they helped me get a great car for cheap.</p></div>
									</div>
								</div>
							</div>

							<!-- Reviews Slider Item -->
							<div class="owl-item">
								<div class="review d-flex flex-row align-items-start justify-content-start">
									<div><div class="review_image"><img src="images/placeholder_woman.png" alt=""></div></div>
									<div class="review_content">
										<div class="review_name">Tay K.</div>
										<div class="review_rating_container">
											<div class="review_rating">@for ($i=1; $i<=5; $i++) <i class="fas fa-star"></i>@endfor</div>
										</div>
										<div class="review_text"><p>Amazing service, friendly employees and clean environment. </p></div>
									</div>
								</div>
							</div>

							<!-- Reviews Slider Item -->
							<div class="owl-item">
								<div class="review d-flex flex-row align-items-start justify-content-start">
									<div><div class="review_image"><img src="images/placeholder_man.png" alt=""></div></div>
									<div class="review_content">
										<div class="review_name">Daniel B.</div>
										<div class="review_rating_container">
											<div class="review_rating">@for ($i=1; $i<=5; $i++) <i class="fas fa-star"></i>@endfor</div>
										</div>
										<div class="review_text"><p>They got my car in amazing condition. The entire process was hassle free as well. Recommend to anyone.</p></div>
									</div>
								</div>
							</div>

						</div>
						<div class="reviews_dots"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Brands -->

	<div class="brands primary-bg">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="brands_slider_container">
						
						<!-- Brands Slider -->

						<div class="owl-carousel owl-theme brands_slider">
							
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/Toyota-logo_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/nissan_PNG63_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/Hyundai-logo-grey_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/Honda-logo_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/cadillac_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/acura_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/audi_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/bmw_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/mercedesbenz_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/dodge_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/infiniti_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/jaguar_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/jeep_edit.png" alt=""></div></div>							
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/porsche_edit.png" alt=""></div></div>
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="images/landrover_edit.png" alt=""></div></div>
						</div>
						
						<!-- Brands Slider Navigation -->
						<div class="brands_nav brands_prev"><i class="fas fa-chevron-left"></i></div>
						<div class="brands_nav brands_next"><i class="fas fa-chevron-right"></i></div>

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection