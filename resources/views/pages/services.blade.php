@extends('layouts/app')

@section('content')
    <!-- Home -->

	<div class="home">
        @foreach($services as $service)@endforeach
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="/storage/content_gallery/{{$service->image}}" data-speed="0.8"></div>
    </div>

    <!-- Single Blog Post -->

    <div class="single_post tertiary-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="single_post_title">{{$service->title}}</div>
                    <div class="single_post_text">
                    {!!$service->body!!}
                </div>
            </div>
        </div>
    </div>

    <!-- Blog Posts -->

    <div class="blog">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="blog_posts d-flex flex-row align-items-start justify-content-between">

                        <div class="blog_post">
                            <div class="blog_image" style="background-image:url(images/trend_oil1.jpg)"></div>
                            <div class="blog_text"><a href="/services/oil-change">OIL CHANGE</a></div>
                        </div>

                        <div class="blog_post">
                            <div class="blog_image" style="background-image:url(images/trend_muffler.jpg)"></div>
                            <div class="blog_text"><a href="/services/exhaust-mufflers">EXHAUST & MUFFLERS</a></div>
                        </div>

                        <div class="blog_post">
                            <div class="blog_image" style="background-image:url(images/trend_brake.jpg)"></div>
                            <div class="blog_text"><a href="/services/brakes">BRAKES</a></div>
                        </div>

                        <div class="blog_post">
                            <div class="blog_image" style="background-image:url(images/trend_wheel1.jpg)"></div>
                            <div class="blog_text"><a href="/services/tires-wheels">TIRES & WHEELS</a></div>
                        </div>

                        <div class="blog_post">
                            <div class="blog_image" style="background-image:url(images/trend_ac.jpg)"></div>
                            <div class="blog_text"><a href="/services/ac">A/C</a></div>
                        </div>

                        <div class="blog_post">
                            <div class="blog_image" style="background-image:url(images/trend_steeringwheel.jpg)"></div>
                            <div class="blog_text"><a href="/services/steering-suspension">STEERING & SUSPENSION</a></div>
                        </div>

                        <div class="blog_post">
                            <div class="blog_image" style="background-image:url(images/trend_battery.jpg)"></div>
                            <div class="blog_text"><a href="/services/car-battery">BATTERIES</a></div>
                        </div>

                        <div class="blog_post">
                            <div class="blog_image" style="background-image:url(images/trend_cv.jpg)"></div>
                            <div class="blog_text"><a href="/services/cv-joints">CV JOINTS</a></div>
                        </div>

                    </div>
                </div>	
            </div>
        </div>
    </div>
@endsection