@extends('layouts/app')

@section('content')
<!-- Home -->

<div class="home">
    @foreach($acContents as $ac)@endforeach
    <div class="home_background parallax-window" data-parallax="scroll" data-image-src="/storage/content_gallery/{{$ac->image}}" data-speed="0.8"></div>
</div>

<!-- Single Blog Post -->

<div class="single_post tertiary-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="single_post_title">{{$ac->title}}</div>
                <div class="single_post_text">
                {!!$ac->body!!}
            </div>
        </div>
    </div>
</div>

<!-- Blog Posts -->

<div class="blog">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="blog_posts d-flex flex-row align-items-start justify-content-between">

                    <!-- Blog post -->
                    <div class="blog_post">
                        <div class="blog_image" style="background-image:url(../images/rc_grahame-jenkins-342518-unsplash.jpg)"></div>
                        <div class="blog_text">How to clean your car's air conditioning system</div>
                    </div>

                    <!-- Blog post -->
                    <div class="blog_post">
                        <div class="blog_image" style="background-image:url(../images/rc_matthieu-joannon-349088-unsplash.jpg)"></div>
                        <div class="blog_text">Planning for a safe and fun fall road trip</div>
                    </div>

                    <!-- Blog post -->
                    <div class="blog_post">
                        <div class="blog_image" style="background-image:url(../images/rc_nikita-kachanovsky-649054-unsplash.jpg)"></div>
                        <div class="blog_text">Understanding your vehicle's cold air intake</div>
                        <!--div class="blog_button"><a href="blog_single.html">Continue Reading</a></div-->
                    </div>

                </div>
            </div>	
        </div>
    </div>
</div>
@endsection