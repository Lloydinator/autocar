@extends('layouts/app')

@section('content')

<!-- Single Blog Post -->

<div class="single_post primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="single_post_title">Software and libraries we used to build this website</div>
                <div class="single_post_text">
                    <ul>
                        <li>
                            <h3><a href="https://laravel.com" target="_blank">Laravel</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://colorlib.com" target="_blank">Colorlib</a></h3>
                        </li>
                        <li>
                            <h3><a href="#">Monster Lite Admin Template</a></h3>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection