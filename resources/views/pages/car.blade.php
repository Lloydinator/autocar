@extends('layouts/app')

@section('content')

    <!-- Single Product -->

	<div class="single_product primary-bg">
        <div class="container">
            <div class="row">

                <!-- Images -->
                <div class="col-lg-2 order-lg-1 order-2">
                    <ul class="image_list">
                        <li data-image="/storage/car_img/{{$car->img}}"><img src="/storage/car_img/{{$car->img}}" alt=""></li>
                        @foreach ($car->gallery as $photo)
                            @if (isset($photo->gallery_photo))
                                <li data-image="/storage/car_gallery/{{$photo->gallery_photo}}"><img src="/storage/car_gallery/{{$photo->gallery_photo}}" alt=""></li>
                            @else
                                <li data-image="/storage/car_gallery/no-image.jpg"><img src="/storage/car_gallery/no-image.jpg" alt=""></li>
                            @endif
                            
                        @endforeach
                    </ul>
                </div>

                <!-- Selected Image -->
                <div class="col-lg-5 order-lg-2 order-1">
                    <div class="image_selected"><img src="/storage/car_img/{{$car->img}}" alt=""></div>
                </div>

                <!-- Description -->
                <div class="col-lg-5 order-3">
                    <div class="product_description">
                        <div class="product_category">{{$car->car_type}}</div>
                        <div class="product_name">{{$car->car_name}}</div>
                        <!--div class="rating_r rating_r_4 product_rating"><i></i><i></i><i></i><i></i><i></i></div-->
                    <div class="product_text"><p>{!!$car->details!!}</p></div>
                    </div>
                    <!-- Attributes -->
                    <div class="attritable">
                            <table class="table-striped">
                                <tr>
                                    <td><strong>Stock #</strong></td>
                                    <td><strong>{{$car->stock_no}}</strong></td>
                                </tr>
                                <tr>
                                    <td>Condition</td>
                                    <td>{{$car->condition}}</td>
                                </tr>
                                <tr>
                                    <td>Year</td>
                                    <td>{{$car->year}}</td>
                                </tr>
                                <tr>
                                    <td>Mileage</td>
                                    <td>{{$car->mileage}}</td>
                                </tr>
                                <tr>
                                    <td>Fuel</td>
                                    <td>{{$car->fuel_type}}</td>
                                </tr>
                                <tr>
                                    <td>Engine</td>
                                    <td>{{$car->engine}} rpm</td>
                                </tr>
                                <tr>
                                    <td>Transmission</td>
                                    <td>{{$car->transmission}}</td>
                                </tr>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bbe5bf5e67eebcd"></script> 
    
@endsection