@extends('layouts/app')

@section('content')
    <!-- Home -->

	<div class="home">
        @foreach($aboutContents as $about)@endforeach
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="/storage/content_gallery/{{$about->image}}" data-speed="0.8"></div>
    </div>

    <!-- Single Blog Post -->

    <div class="single_post tertiary-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="single_post_title">{{$about->title}}</div>
                    <div class="single_post_text">
                    <p>{!!$about->body!!}</p>
                </div>
            </div>
        </div>
    </div>
@endsection