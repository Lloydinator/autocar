@extends('layouts/app')

@section('content')
<div class="text-center">
    @if (count($privacies)>0)
        @foreach ($privacies as $privacy)
            <h1>{{$privacy->title}}</h1>
            <div>
                {!!$privacy->body!!}
            </div>
        @endforeach
    @endif
</div>
@endsection