<!-- Header -->
<header class="header">
    <!-- Header Main -->

    <div class="header_main tertiary-bg">
        <div class="container">
            <div class="row">

                <!-- Logo -->
                <div class="col-lg col-12 order-1">
                    <div class="logo_container">
                        <div class="logo">
                            <a href="/"><img src="{{ asset('images/autotrend.png') }}" alt=""><span>uto Trend</span></a>
                            <p><strong><i>Motor Cars and Collision Services</i></strong></p>
                        </div>
                    </div>
                </div>

                <!-- Search -->
                <div class="col-lg col-12 order-lg-2 order-3 text-lg-left text-right">
                    <div class="header_search">
                        <div class="header_search_content">
                            <div class="header_search_form_container">
                                <form action="#" class="header_search_form clearfix">
                                    <input type="search" required="required" class="header_search_input" placeholder="Search for cars...">
                                    <div class="custom_dropdown">
                                        <div class="custom_dropdown_list">
                                            <span class="custom_dropdown_placeholder clc">All Categories</span>
                                            <i class="fas fa-chevron-down"></i>
                                            <ul class="custom_list clc">
                                                <li><a class="clc" href="/cars">All Categories</a></li>
                                                <li><a class="clc" href="/cars/sedans">Sedans</a></li>
                                                <li><a class="clc" href="/cars/hybrids">Hybrids</a></li>
                                                <li><a class="clc" href="/cars/compact">Compact</a></li>
                                                <li><a class="clc" href="/cars/electric">Electric</a></li>
                                                <li><a class="clc" href="/cars/convertibles">Convertibles</a></li>
                                                <li><a class="clc" href="/cars/luxury">Luxury</a></li>
                                                <li><a class="clc" href="/cars/crossovers">Crossovers</a></li>
                                                <li><a class="clc" href="/cars/suvs">SUVs</a></li>
                                                <li><a class="clc" href="/cars/vans">Vans</a></li>
                                                <li><a class="clc" href="/cars/trucks">Trucks</a></li>                                               
                                            </ul>
                                        </div>
                                    </div>
                                    <button type="submit" class="header_search_button trans_300" value="Submit"><img src="{{ asset('images/search.png') }}" alt=""></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
    <!-- Main Navigation -->

    <nav class="main_nav offblack">
        <div class="container">
            <div class="row">
                <div class="col">
                    
                    <div class="main_nav_content d-flex flex-row">

                        <!-- Categories Menu -->

                        <div class="cat_menu_container">
                            <div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
                                <div class="cat_burger"><span></span><span></span><span></span></div>
                                <div class="cat_menu_text">categories</div>
                            </div>

                            <ul class="cat_menu">
                                <li><a href="/cars">All Categories <i class="fas fa-chevron-right ml-auto"></i></a></li>
                                <li><a href="/cars/sedans">Sedans<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="/cars/hybrids">Hybrids<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="/cars/compact">Compact<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="/cars/electric">Electric<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="/cars/convertibles">Convertibles<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="/cars/luxury">Luxury<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="/cars/crossovers">Crossovers<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="/cars/suvs">SUVs<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="/cars/vans">Vans<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="/cars/trucks">Trucks<i class="fas fa-chevron-right"></i></a></li>
                            </ul>
                        </div>

                        <!-- Main Nav Menu -->

                        <div class="main_nav_menu ml-auto">
                            <ul class="standard_dropdown main_nav_dropdown">
                                <li><a href="/">Home<i class="fas fa-chevron-down"></i></a></li>
                                <li><a href="/services">Services<i class="fas fa-chevron-down"></i></a></li>
                                <li><a href="/about">About<i class="fas fa-chevron-down"></i></a></li>
                                <li><a href="/cars">Inventory<i class="fas fa-chevron-down"></i></a></li>
                                <li><a href="/contact">Contact<i class="fas fa-chevron-down"></i></a></li>
                                <li><a href="https://iaai.com" target="_blank">Auction<i class="fas fa-chevron-down"></i></a></li>
                            </ul>
                        </div>

                        <!-- Menu Trigger -->

                        <div class="menu_trigger_container ml-auto">
                            <div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
                                <div class="menu_burger">
                                    <div class="menu_trigger_text">menu</div>
                                    <div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </nav>
    
    <!-- Menu -->

    <div class="page_menu">
        <div class="container">
            <div class="row">
                <div class="col">
                    
                    <div class="page_menu_content">
                        
                        <div class="page_menu_search">
                            <form action="#">
                                <input type="search" required="required" class="page_menu_search_input" placeholder="Search for cars...">
                            </form>
                        </div>
                        <ul class="page_menu_nav">
                            <li class="page_menu_item has-children">
                                <a href="#">Language<i class="fa fa-angle-down"></i></a>
                                <ul class="page_menu_selection">
                                    <li><a href="#">English<i class="fa fa-angle-down"></i></a></li>
                                    <li><a href="#">Italian<i class="fa fa-angle-down"></i></a></li>
                                    <li><a href="#">Spanish<i class="fa fa-angle-down"></i></a></li>
                                    <li><a href="#">Japanese<i class="fa fa-angle-down"></i></a></li>
                                </ul>
                            </li>
                            <li class="page_menu_item">
                                <a href="#">Home<i class="fa fa-angle-down"></i></a>
                            </li>
                            <li class="page_menu_item"><a href="/services">services<i class="fa fa-angle-down"></i></a></li>
                            <li class="page_menu_item"><a href="/about">about<i class="fa fa-angle-down"></i></a></li>
                            <li class="page_menu_item"><a href="/cars">Inventory<i class="fa fa-angle-down"></i></a></li>
                            <li class="page_menu_item"><a href="/contact">contact<i class="fa fa-angle-down"></i></a></li>
                            <li class="page_menu_item"><a href="https://iaai.com" target="_blank">auction<i class="fa fa-angle-down"></i></a></li>
                        </ul>
                        
                        <div class="menu_contact">
                            <div class="menu_contact_item"><div class="menu_contact_icon"><img src="{{ asset('images/phone_white.png') }}" alt=""></div><a href="tel:7184050888">(718) 405-0888</a></div>
                            <div class="menu_contact_item"><div class="menu_contact_icon"><img src="{{ asset('images/mail_white.png') }}" alt=""></div><a href="mailto:roy4autotrend@gmail.com">roy4autotrend@gmail.com</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>