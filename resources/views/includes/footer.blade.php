<footer class="footer">
    <div class="container">
        <div class="row">

            <div class="col-lg-3 footer_col">
                <div class="footer_column footer_contact">
                    <div class="logo_container">
                        <div class="logo"><a href="#">Auto Trend</a></div>
                    </div>
                    <div class="footer_title">Got a Question? Call Us</div>
                    <div class="footer_phone"><a href="tel:7184050888">(718) 405-0888</a></div>
                    <div class="footer_contact_text">
                        <p>712 E 240th St</p>
                        <p>Bronx, NY 10470</p>
                    </div>
                    <div class="footer_social">
                        <ul>
                            <li><a href="https://facebook.com"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="https://twitter.com"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="https://instagram.com"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 offset-lg-2">
                <div class="footer_column">
                    <div class="footer_title">Our Services</div>
                    <ul class="footer_list">
                        <li><a href="/services/ac">A/C</a></li>
                        <li><a href="/services/car-battery">Car Battery</a></li>
                        <li><a href="/services/brakes">Brakes</a></li>
                        <li><a href="/services/cv-joints">CV Joints</a></li>
                        <li><a href="/services/exhaust-mufflers">Exhaust & Mufflers</a></li>
                        <li><a href="/services/oil-change">Oil Change</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="footer_column">
                    <ul class="footer_list footer_list_2">
                            <li><a href="/services/steering-suspension">Steering & Suspension</a></li>
                        <li><a href="/services/tires-wheels">Tires & Wheels</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="footer_column">
                    <div class="footer_title">Customer Care</div>
                    <ul class="footer_list">
                        <li><a href="/">My Account</a></li>
                        <li><a href="/third-party">Third Party Software</a></li>
                        <li><a href="/services">Customer Services</a></li>
                        <li><a href="/about">FAQs</a></li>
                        <li><a href="/contact">Customer Support</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</footer>

<!-- Copyright -->

<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col">
                
                <div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start">
                    <div class="copyright_content">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This website is powered by <a href="https://webcondo.co" target="_blank">WebCondo</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
                    <div class="logos ml-sm-auto">
                        <ul class="logos_list">
                            <li><a href="#"><img src="{{ asset('images/logos_1.png') }}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('images/logos_2.png') }}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('images/logos_3.png') }}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('images/logos_4.png') }}" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="outdated"></div>