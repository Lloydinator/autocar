<!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li>
                        <a href="/admin" class="waves-effect"><i class="fa fa-clock-o m-r-10" aria-hidden="true"></i>Home</a>
                    </li>
                    <li>
                        <a href="/admin/cars" class="waves-effect"><i class="fa fa-table m-r-10" aria-hidden="true"></i>Cars</a>
                    </li>
                    <li>
                        <a href="/admin/content" class="waves-effect"><i class="fa fa-table m-r-10" aria-hidden="true"></i>Pages</a>
                    </li>
                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->