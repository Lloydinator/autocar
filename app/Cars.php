<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    protected $table = 'cars';

    public function gallery(){
        return $this->hasMany('App\Gallery','carsid');
    }
}
