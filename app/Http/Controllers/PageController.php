<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\Cars;
use App\Gallery;

class PageController extends Controller
{
    public function index(){
        $allTheCars = Cars::all();
        return view('pages/index')->with('cars',$allTheCars);
    }

    public function about(){
        $aboutContents = Content::where('page','about')->get();
        return view('pages/about')->with('aboutContents',$aboutContents);
    }

    public function contact(){
        $contactContents = Content::where('page','contact')->get();
        return view('pages/contact')->with('contact',$contactContents);
    }

    public function privacy(){
        $privacyContents = Content::where('page','privacy')->get();
        return view('pages/privacy')->with('privacies',$privacyContents);
    }

    public function services(){
        $servicesContents = Content::where('page','services')->get();
        return view('pages/services')->with('services',$servicesContents);
    }
    public function mufflers(){
        $mufflersContents = Content::where('page','services-mufflers')->get();
        return view('pages/services/mufflers')->with('mufflersContents',$mufflersContents);
    }
    public function oil(){
        $oilContents = Content::where('page','services-oil')->get();
        return view('pages/services/oil')->with('oilContents',$oilContents);
    }
    public function brakes(){
        $brakesContents = Content::where('page','services-brakes')->get();
        return view('pages/services/brakes')->with('brakesContents',$brakesContents);
    }
    public function wheels(){
        $wheelsContents = Content::where('page','services-wheels')->get();
        return view('pages/services/wheels')->with('wheelsContents',$wheelsContents);
    }
    public function ac(){
        $acContents = Content::where('page','services-ac')->get();
        return view('pages/services/ac')->with('acContents',$acContents);
    }
    public function steering(){
        $steeringContents = Content::where('page','services-steering')->get();
        return view('pages/services/steering')->with('steeringContents',$steeringContents);
    }
    public function battery(){
        $batteryContents = Content::where('page','services-battery')->get();
        return view('pages/services/battery')->with('batteryContents',$batteryContents);
    }
    public function cv(){
        $cvContents = Content::where('page','services-cv')->get();
        return view('pages/services/cv')->with('cvContents',$cvContents);
    }


    
    public function terms(){
        $termsContents = Content::where('page','terms')->get();
        return view('pages/terms')->with('terms',$termsContents);
    }

    public function cars(){
        $allCars = Cars::all();
        return view('pages/cars')->with('cars',$allCars);
    }
    public function thirdparty(){
        $thirdParty = Content::where('page','third-party')->get();
        return view('pages/thirdparty')->with('thirdparties',$thirdParty);
    }
    /*
    public function cars(Request $request, $category){
        if ($category == 'Sedans'){
            $sedans = Cars::where('car_type','Sedan');
            return view('pages/cars')->with('cars',$sedans);
        }
        else {
            $allCars = Cars::all();
            return view('pages/cars')->with('cars',$allCars);
        }
    }
    */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $car = Cars::with('Gallery')->find($id);
        return view('pages/car')->with('car',$car);
    }



    public function sedans(){
        $sedans = Cars::where('car_type','Sedan')->get();
        return view('categories/sedans')->with('sedans',$sedans);
    }

    public function hybrids(){
        $hybrids = Cars::where('car_type','Hybrid')->get();
        return view('categories/hybrids')->with('hybrids',$hybrids);
    }

    public function electric(){
        $electrics = Cars::where('car_type','Electric')->get();
        return view('categories/electric')->with('electrics',$electrics);
    }

    public function convertibles(){
        $convertibles = Cars::where('car_type','Convertible')->get();
        return view('categories/convertibles')->with('convertibles',$convertibles);
    }

    public function luxury(){
        $luxuries = Cars::where('car_type','Luxury')->get();
        return view('categories/luxury')->with('luxuries',$luxuries);
    }

    public function crossovers(){
        $crossovers = Cars::where('car_type','Crossover')->get();
        return view('categories/crossovers')->with('crossovers',$crossovers);
    }

    public function compact(){
        $compacts = Cars::where('car_type','Compact')->get();
        return view('categories/compact')->with('compacts',$compacts);
    }

    public function suvs(){
        $suvs = Cars::where('car_type','Small SUV')->get();
        return view('categories/suvs')->with('suvs',$suvs);
    }

    public function vans(){
        $vans = Cars::where('car_type','Van')->get();
        return view('categories/vans')->with('vans',$vans);
    }

    public function trucks(){
        $trucks = Cars::where('car_type','Pickup Truck')->get();
        return view('categories/trucks')->with('trucks',$trucks);
    }
}



