<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Gallery;
use App\Cars;

class GalleryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function create($id)
    {
        return view('gallery/create')->with('car_id',$id);  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('gallery_image')){
            //Get full filename
            $fullName = $request->file('gallery_image')->getClientOriginalName();

            //Get filename
            $fileName = pathinfo($fullName,PATHINFO_FILENAME);

            //Get ext
            $ext = $request->file('gallery_image')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = $fileName.'_'.rand(1000,99999).'.'.$ext;

            //Upload image
            $path = $request->file('gallery_image')->storeAs('public/car_gallery',$fileNameToStore);
        }

        $photo = new Gallery;
        $photo->carsid = $request->input('car_id');
        $photo->title = $request->input('title');
        $photo->caption = $request->input('caption');
        if ($request->hasFile('gallery_image')){
            $photo->gallery_photo = $fileNameToStore;
        }
        $photo->save();
        return redirect('admin/cars/gallery/'.$request->input('car_id'))->with('success','Another photo added to gallery');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Gallery::find($id);

        Storage::delete('public/car_img/'.$photo->img);

        $photo->delete();
        return redirect('/admin/cars/')->with('success','Image removed!');
    }
}
