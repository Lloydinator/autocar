<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Cars;
use App\Gallery;

class CarsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Cars::all();
        return view('cars/index')->with('cars',$cars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cars/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'type' => 'required',
            'name' => 'required',
            'price' => 'required',
            'details' => 'required',
            'mileage' => 'required',
            'fuel' => 'required',
            'engine' => 'required',
            'transmission' => 'required',
            'stock' => 'required'
        ]);

        if ($request->hasFile('car_img')){
            //Get filename
            $filenameWithExt = $request->file('car_img')->getClientOriginalName();

            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            //Get just ext
            $ext = $request->file('car_img')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$ext;

            //Upload image
            $path = $request->file('car_img')->storeAs('public/car_img',$fileNameToStore);
        }
        /*
        if ($request->hasFile('gallery')){
            foreach($request->file('gallery') as $galleryFile){
                //Get filename
                $galleryNameWithExt = $galleryFile->getClientOriginalName();
                //Get just filename
                $galleryName = pathinfo($galleryNameWithExt, PATHINFO_FILENAME);
                //Get just ext
                $galleryExt = $galleryFile->getClientOriginalExtension();
                //Filename to store
                $galleryNameToStore = $galleryName.'_'.time().'.'.$galleryExt;
                //Upload image
                $galleryFile->storeAs('public/car_gallery',$galleryNameToStore);
                
                $galleryData[] = $galleryNameToStore;
            }
            
        }
        */
        $car = new Cars;
        $car->car_type = $request->input('type');
        $car->car_name = $request->input('name');
        $car->price = $request->input('price');
        $car->year = $request->input('year');
        $car->condition = $request->input('condition');
        $car->details = $request->input('details');
        $car->mileage = $request->input('mileage');
        $car->fuel_type = $request->input('fuel');
        $car->engine = $request->input('engine');
        $car->transmission = $request->input('transmission');
        $car->stock_no = $request->input('stock');
        if ($request->hasFile('car_img')){
            $car->img = $fileNameToStore;
        }
        $car->save();
        return redirect('/admin/cars/')->with('success','New car added!');
    }

    public function show($id)
    {
        $car = Cars::with('Gallery')->find($id);
        return view('gallery/index')->with('car',$car);
    }


    public function edit($id)
    {
        $car = Cars::find($id);
        return view('cars/edit')->with('car',$car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'type' => 'required',
            'name' => 'required',
            'price' => 'required',
            'details' => 'required',
            'mileage' => 'required',
            'fuel' => 'required',
            'engine' => 'required',
            'transmission' => 'required',
            'stock' => 'required'
        ]);
        
        /*
        if ($request->hasFile('gallery')){
            foreach($request->file('gallery') as $galleryFile){
                //Get filename
                $galleryNameWithExt = $galleryFile->getClientOriginalName();
                //Get just filename
                $galleryName = pathinfo($galleryNameWithExt, PATHINFO_FILENAME);
                //Get just ext
                $galleryExt = $galleryFile->getClientOriginalExtension();
                //Filename to store
                $galleryNameToStore = $galleryName.'_'.time().'.'.$galleryExt;
                //Upload image
                $galleryFile->storeAs('public/car_gallery',$galleryNameToStore);
                $galleryData[] = $galleryNameToStore;
            }
        }
        */
        
        if ($request->hasFile('car_img')){
            //Get filename
            $filenameWithExt = $request->file('car_img')->getClientOriginalName();

            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            //Get just ext
            $ext = $request->file('car_img')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$ext;

            //Upload image
            $path = $request->file('car_img')->storeAs('public/car_img',$fileNameToStore);
        }
        $car = Cars::find($id);
        $car->car_type = $request->input('type');
        $car->car_name = $request->input('name');
        $car->price = $request->input('price');
        $car->year = $request->input('year');
        $car->condition = $request->input('condition');
        $car->details = $request->input('details');
        $car->mileage = $request->input('mileage');
        $car->fuel_type = $request->input('fuel');
        $car->engine = $request->input('engine');
        $car->transmission = $request->input('transmission');
        $car->stock_no = $request->input('stock');
        if ($request->hasFile('car_img')){
            $car->img = $fileNameToStore;
        }
        //$car->cars_gallery = json_encode($galleryData);
        $car->save();

        return redirect('admin/cars')->with('success','You just edited your post!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Cars::find($id);

        //Don't delete noimage image
        if ($car->img != 'noimage.jpg'){
            Storage::delete('public/car_img/'.$car->img);
        }

        $car->delete();
        return redirect('/admin/cars')->with('success','Car removed');
    }
}
