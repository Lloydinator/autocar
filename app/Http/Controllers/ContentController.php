<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;

class ContentController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents = Content::all();
        return view('content/index')->with('contents',$contents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
            'content-page' => 'required'
        ]);

        if ($request->hasFile('image')){
            //Get full filename
            $fullName = $request->file('image')->getClientOriginalName();

            //Get filename
            $fileName = pathinfo($fullName,PATHINFO_FILENAME);

            //Get ext
            $ext = $request->file('image')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = $fileName.'_'.rand(1000,99999).'.'.$ext;

            //Upload image
            $path = $request->file('image')->storeAs('public/content_gallery',$fileNameToStore);
        }

        $content = new Content;
        $content->title = $request->input('title');
        $content->body = $request->input('body');
        $content->page = $request->input('content-page');
        if ($request->hasFile('image')){
            $content->image = $fileNameToStore;
        }
        $content->save();
        return redirect('/admin/content')->with('success','Content created ;)');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //PageController already shows this content
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $content = Content::find($id);

        return view('content/edit')->with('content',$content);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required'
        ]);

        if ($request->hasFile('image')){
            //Get full filename
            $fullName = $request->file('image')->getClientOriginalName();

            //Get filename
            $fileName = pathinfo($fullName,PATHINFO_FILENAME);

            //Get ext
            $ext = $request->file('image')->getClientOriginalExtension();

            //Filename to store
            $fileNameToStore = $fileName.'_'.rand(1000,99999).'.'.$ext;

            //Upload image
            $path = $request->file('image')->storeAs('public/content_gallery',$fileNameToStore);
        }

        $content = Content::find($id);
        $content->title = $request->input('title');
        $content->body = $request->input('body');
        if ($request->hasFile('image')){
            $content->image = $fileNameToStore;
        }
        $content->save();

        return redirect('admin/content/')->with('success','Content updated :)');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $content = Content::find($id);

        $content->delete();
        return redirect('admin/content/')->with('success','Content deleted :(');
    }
}
